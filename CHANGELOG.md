# CHANGELOG



## v0.1.0 (2024-01-14)

### Feature

* feat: initiatl commit ([`9c66cd7`](https://gitlab.com/aborsu/example-python-semantic-release/-/commit/9c66cd705562dd8e2600ad45b0b8a56ccb9ce94a))

### Unknown

* Configure SAST in `.gitlab-ci.yml`, creating this file if it does not already exist ([`a9f46e4`](https://gitlab.com/aborsu/example-python-semantic-release/-/commit/a9f46e4b4c60913a4d5f8654ab88105ccff5bf10))

* Initial commit ([`7221afc`](https://gitlab.com/aborsu/example-python-semantic-release/-/commit/7221afcd5bd8cf94992e2d5e28ded637a8e56f68))
